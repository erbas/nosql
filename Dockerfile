FROM maven:3.6.1-jdk-11-slim AS build
WORKDIR /tmp
COPY . .
RUN mvn package --no-transfer-progress -DskipTests


FROM adoptopenjdk/openjdk11

COPY --from=build /tmp/target/mongoPoC-0.0.1-SNAPSHOT.jar app.jar

EXPOSE 8080
CMD [ "-jar", "app.jar" ]
ENTRYPOINT [ "java" ]