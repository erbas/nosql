package mongoPoC.mongoPoC;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MongoPoCApplication {
	public static void main(String[] args) {
		SpringApplication.run(MongoPoCApplication.class, args);
	}

}
