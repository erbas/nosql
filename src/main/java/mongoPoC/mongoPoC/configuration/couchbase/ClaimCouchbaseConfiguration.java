package mongoPoC.mongoPoC.configuration.couchbase;

import com.couchbase.client.java.Bucket;
import com.couchbase.client.java.Cluster;
import com.couchbase.client.java.CouchbaseCluster;
import com.couchbase.client.java.env.CouchbaseEnvironment;
import com.couchbase.client.java.env.DefaultCouchbaseEnvironment;
import mongoPoC.mongoPoC.model.ClaimDocument;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.couchbase.config.AbstractCouchbaseConfiguration;
import org.springframework.data.couchbase.core.CouchbaseTemplate;
import org.springframework.data.couchbase.core.convert.MappingCouchbaseConverter;
import org.springframework.data.couchbase.repository.config.RepositoryOperationsMapping;

import java.util.Arrays;
import java.util.List;

@Configuration
public class ClaimCouchbaseConfiguration extends AbstractCouchbaseConfiguration {

    @Value("${spring.couchbase.bootstrap-hosts}")
    private String host;

    @Value("${spring.couchbase.rest.port:8091}")
    private int port;

    @Value("${spring.couchbase.carrier.port:11210}")
    private int carrierPort;

    @Value("${spring.couchbase.bucket.password}")
    private String bucketPassword;

    @Value("${spring.couchbase.bucket.name}")
    private String bucketName;

    @Override
    protected List<String> getBootstrapHosts() {
        return Arrays.asList(host);
    }

    @Override
    protected String getBucketName() {
        return bucketName;
    }

    @Override
    protected String getBucketPassword() {
        return bucketPassword;
    }

    @Override
    public void configureRepositoryOperationsMapping(RepositoryOperationsMapping baseMapping) {
        baseMapping.mapEntity(ClaimDocument.class, claimCouchbaseTemplate());
    }

    @Bean
    public Bucket claimBucket() {
        return claimCluster().openBucket(bucketName, bucketPassword);
    }

    @Bean
    public CouchbaseTemplate claimCouchbaseTemplate() {
        CouchbaseTemplate template;

        try {
            template = getCouchbaseTemplate(claimBucket(), mappingCouchbaseConverter());
            template.setDefaultConsistency(getDefaultConsistency());

        } catch (Exception e) {
            throw new IllegalArgumentException(e);
        }
        return template;
    }

    @Bean
    public Cluster claimCluster() {
        return CouchbaseCluster.create(couchbaseEnvironment(), host);
    }

    private CouchbaseTemplate getCouchbaseTemplate(Bucket client, MappingCouchbaseConverter mappingCouchbaseConverter) throws Exception {
        CouchbaseTemplate template = new CouchbaseTemplate(
                couchbaseClusterInfo(),
                client,
                mappingCouchbaseConverter,
                translationService()
        );
        template.setDefaultConsistency(getDefaultConsistency());

        return template;
    }

    @Override
    protected CouchbaseEnvironment getEnvironment() {
        return DefaultCouchbaseEnvironment.builder()
                .connectTimeout(20000)
                .kvTimeout(15000)
                .build();
    }
}
