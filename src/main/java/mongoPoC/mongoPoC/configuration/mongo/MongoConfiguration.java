package mongoPoC.mongoPoC.configuration.mongo;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import mongoPoC.mongoPoC.model.ClaimDocument;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.codecs.pojo.PojoCodecProvider;
import org.springframework.context.annotation.Bean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;


import static org.bson.codecs.configuration.CodecRegistries.fromProviders;
import static org.bson.codecs.configuration.CodecRegistries.fromRegistries;

@Configuration
public class MongoConfiguration {

    @Value("${spring.mongo.connectionString:mongodb://localhost:27017}")
    private String connectionString;

    @Value("${spring.mongo.databaseName:claim}")
    private String databaseName;

    @Value("${spring.mongo.collections.claim:claims}")
    private String claimCollectionName;



    @Bean
    public MongoCollection<ClaimDocument> claimDocumentMongoCollection(MongoClient claimMongoClient) {

        CodecRegistry pojoCodecRegistry = fromRegistries(MongoClient.getDefaultCodecRegistry(),
                fromProviders(PojoCodecProvider.builder().automatic(true).build()));


        MongoDatabase database = claimMongoClient.getDatabase(this.databaseName);
        MongoCollection<ClaimDocument> collection = database.getCollection(this.claimCollectionName, ClaimDocument.class);
        MongoCollection<ClaimDocument> claimDocumentMongoCollection = collection.withCodecRegistry(pojoCodecRegistry);
        return claimDocumentMongoCollection;
    }

    @Bean
    public MongoClient claimMongoClient() {

        MongoClientURI connectionString = new MongoClientURI(this.connectionString);

        MongoClient mongoClient = new MongoClient(connectionString);

        return mongoClient;
    }
}
