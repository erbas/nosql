package mongoPoC.mongoPoC.controller;

import com.flextrade.jfixture.JFixture;
import mongoPoC.mongoPoC.model.ClaimDocument;
import mongoPoC.mongoPoC.repository.ClaimRepositoryCouchbase;
import mongoPoC.mongoPoC.repository.ClaimRepositoryMongo;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Random;


@RestController
@RequestMapping("/claims")
public class ClaimController {

    private final ClaimRepositoryMongo claimRepository;
    private final ClaimRepositoryCouchbase claimRepositoryCouchbase;


    @Autowired
    public ClaimController(ClaimRepositoryMongo claimRepository, ClaimRepositoryCouchbase claimRepositoryCouchbase) {
        this.claimRepository = claimRepository;
        this.claimRepositoryCouchbase = claimRepositoryCouchbase;
    }

    @GetMapping("/mongo/getLastMonthRandomStatuses/{monthSpan}")
    public void mgetRandomLastMonth(@PathVariable Integer monthSpan) {
        DateTime now = new DateTime(1563988685000L);
        var lastMonth = now.minusMonths(monthSpan);
        claimRepository.getBy(lastMonth.getMillis(), now.getMillis(), getRandomList());
    }

    @GetMapping("/couchbase/getLastMonthRandomStatuses/{monthSpan}")
    public void cgetRandomLastMonth(@PathVariable Integer monthSpan) {
        DateTime now = new DateTime(1563988685000L);
        var lastMonth = now.minusMonths(monthSpan);
        claimRepositoryCouchbase.getBy(lastMonth.getMillis(), now.getMillis(), getRandomList());
    }

    @GetMapping("/mongo/read") //read by random generated claim item status history status
    public void mread() {
        Integer generatedLong = new Random().nextInt(8);
        claimRepository.getByClaimItemStatusHistoryStatus(generatedLong);
    }

    @GetMapping("/couchbase/read") //read by random generated claim item status history status
    public void cread() {
        Integer generatedLong = new Random().nextInt(8);
        claimRepositoryCouchbase.getByClaimItemStatusHistoryStatus(15, generatedLong);
    }

    @GetMapping("/mongo/read-update-tx")
    //read by random generated claim item status history status and increment that status by one and save
    public void RUTx() {
        Integer generatedLong = new Random().nextInt(8);
        claimRepository.getByClaimItemStatusHistoryStatusandIncrement1WithTx(generatedLong);

    }

    @GetMapping("/mongo/read-update")
    //read by random generated claim item status history status and increment that status by one and save
    public void RU() {
        Integer generatedLong = new Random().nextInt(8);
        claimRepository.getByClaimItemStatusHistoryStatusandIncrement1(generatedLong);

    }

    @GetMapping("/couchbase/read-update")
    //read by random generated claim item status history status and increment that status by one and save
    public void CRU() {
        Integer generatedLong = new Random().nextInt(8);
        claimRepositoryCouchbase.getByIncrement1(generatedLong);
    }


    @GetMapping("/mongo/idRead")
    //read by random generated claim item status history status and increment that status by one and save
    public void mongo() throws Exception {
        Integer generatedLong = new Random().nextInt(25000);
        claimRepository.getBy((long) generatedLong);
    }

    @GetMapping("/couchbase/idRead")
    //read by random generated claim item status history status and increment that status by one and save
    public void cir() {
        Integer generatedLong = new Random().nextInt(25000);
        claimRepositoryCouchbase.fetchById((long) generatedLong);
    }

    @GetMapping("/insertCilginca/{count}") //insert to both couchbase and mongo count*25000 documents
    public void fafa(@PathVariable Long count) {

        System.out.println("Clear Databases first");
//        claimRepository.clearAll();
//        claimRepositoryCouchbase.clearAll();

        Long documentCountPerInsertLoop = 2500L;
        Long nanoToMiliSecondDivider = 1000000L;
        for (Long x = 0L; x < count; x++) {
            List<ClaimDocument> claimDocuments = new ArrayList<ClaimDocument>();
            JFixture fixture = new JFixture();

            for (Long i = x * documentCountPerInsertLoop; i < documentCountPerInsertLoop * (x + 1); i++) {
                DateTime now = new DateTime();
                ClaimDocument ff = fixture.create(ClaimDocument.class);
                ff.setOrderId(i);
                ff.setId(i);
                ff.getClaimItems().forEach(claimItemDocument -> {
                    claimItemDocument.setClaimItemStatus(new Random().nextInt(8));
                    claimItemDocument.getClaimItemStatusHistories().forEach(claimItemStatusHistoryDocument -> {
                        claimItemStatusHistoryDocument.setClaimItemStatus(new Random().nextInt(8));
                    });
                });
                var date = now.minusMonths((int) (i % 8));
                var date2 = date.minusDays((int) (i % 20));
                ff.setLastModifiedDate(date2.getMillis());
                claimDocuments.add(ff);
            }

            System.out.println("CouchBase insert start ");
            long startTime = System.nanoTime();
            claimDocuments.forEach(claimDocument -> claimRepositoryCouchbase.insert(claimDocument));

            long endTime = System.nanoTime();
            long timeElapsed = endTime - startTime;

            System.out.println("CouchBase insert Execution time in milliseconds : " +
                    (float) timeElapsed / nanoToMiliSecondDivider);

            System.out.println("Couchbase each insert time in milliseconds : " +
                    (float) timeElapsed / (nanoToMiliSecondDivider * documentCountPerInsertLoop));

            System.out.println("mongo insert start ");
            long startTime2 = System.nanoTime();
            claimDocuments.forEach(claimDocument -> claimRepository.insert(claimDocument));
            long endTime2 = System.nanoTime();
            long timeElapsed2 = endTime2 - startTime2;

            System.out.println("mongo insert Execution time in milliseconds : " +
                    (float) timeElapsed2 / nanoToMiliSecondDivider);
            System.out.println("Mongo each insert time in milliseconds : " +
                    (float) timeElapsed2 / (nanoToMiliSecondDivider * documentCountPerInsertLoop));
        }

    }

    private List<Integer> getRandomList() {
        Integer count = new Random().nextInt(4) + 1;
        var itemStatuses = new HashSet<Integer>();
        for (int i = 0; i < count; i++) {
            itemStatuses.add(new Random().nextInt(8));
        }
        return new ArrayList<>(itemStatuses);
    }
}