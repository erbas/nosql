package mongoPoC.mongoPoC.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/probe")
public class ProbeController {

    @GetMapping("/live")
    public String live() {
        return "OK";
    }

    @GetMapping("/ready")
    public String ready() {
        return "OK";
    }


}
