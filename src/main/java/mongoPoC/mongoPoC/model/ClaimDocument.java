package mongoPoC.mongoPoC.model;

import com.flextrade.jfixture.annotations.Fixture;
import com.flextrade.jfixture.annotations.Range;
import org.springframework.data.annotation.Id;
import org.springframework.data.couchbase.core.mapping.Document;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Document
public class ClaimDocument implements Serializable {

    @Id
    private Long id;

    private Long createdDate;

    private Long orderId;

    private String orderParentNumber;

    private Integer fulfillmentTypeId;

    @Fixture
    @Range(min = 0L, max = 2000L)
    private Long supplierId;

    private Long cargoTrackingNumber;

    private List<ClaimItemDocument> claimItems;

    private Long lastModifiedDate;

    private List<Long> shipmentPackageIds;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Long createdDate) {
        this.createdDate = createdDate;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public String getOrderParentNumber() {
        return orderParentNumber;
    }

    public void setOrderParentNumber(String orderParentNumber) {
        this.orderParentNumber = orderParentNumber;
    }

    public Integer getFulfillmentTypeId() {
        return fulfillmentTypeId;
    }

    public void setFulfillmentTypeId(Integer fulfillmentTypeId) {
        this.fulfillmentTypeId = fulfillmentTypeId;
    }

    public Long getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(Long supplierId) {
        this.supplierId = supplierId;
    }

    public Long getCargoTrackingNumber() {
        return cargoTrackingNumber;
    }

    public void setCargoTrackingNumber(Long cargoTrackingNumber) {
        this.cargoTrackingNumber = cargoTrackingNumber;
    }

    public List<ClaimItemDocument> getClaimItems() {
        return claimItems;
    }

    public void setClaimItems(List<ClaimItemDocument> claimItems) {
        this.claimItems = claimItems;
    }

    public Long getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Long lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public List<Long> getShipmentPackageIds() {
        return shipmentPackageIds;
    }

    public void setShipmentPackageIds(List<Long> shipmentPackageIds) {
        this.shipmentPackageIds = shipmentPackageIds;
    }
}
