package mongoPoC.mongoPoC.model;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Getter
@Setter
public class ClaimIssueDocument implements Serializable {

    private String claimIssueReason;

    private List<ClaimIssueFileDocument> claimIssueFiles;

    private String description;

    private Long id;

    private Date issueDate;

    private Integer claimIssueReasonId;
}
