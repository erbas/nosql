package mongoPoC.mongoPoC.model;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class ClaimIssueFileDocument implements Serializable {

    private String fileName;

    private Long id;
}
