package mongoPoC.mongoPoC.model;


import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Getter
@Setter
public class ClaimItemDocument implements Serializable {

    private Long id;

    private String customerReasonDescriptionAvailable;

    private Date autoApproveDate;

    private Date lastModifiedDate;

    private Integer platformReasonExternalId;

    private String claimItemNoteModifierUser;

    private Integer customerReasonExternalId;

    private List<ClaimItemStatusHistoryDocument> claimItemStatusHistories;

    private Integer customerClaimItemReasonId;

    private Long orderLineItemId;

    private String platformClaimItemReasonId;

    private Integer customerReasonExternalCode;

    private String customerReasonName;

    private String claimItemNote;

    private Integer claimItemStatus;

    private Date createdDate;

    private String customerNote;

    private Integer orderLineItemStatus;

    private String platformReasonName;

    private ClaimIssueDocument claimIssue;

    private Integer platformReasonExternalCode;
}