package mongoPoC.mongoPoC.model;

import com.flextrade.jfixture.annotations.Fixture;
import com.flextrade.jfixture.annotations.Range;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
public class ClaimItemStatusHistoryDocument implements Serializable {

    @Fixture // You still need to include this annotation
    @Range(min = 0, max = 20)
    private Integer claimItemStatus;

    private String executorUser;

    private Date createdDate;

    private Long executorId;

    private String executorApp;
}
