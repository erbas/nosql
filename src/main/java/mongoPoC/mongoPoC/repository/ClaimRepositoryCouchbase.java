package mongoPoC.mongoPoC.repository;

import com.couchbase.client.java.document.json.JsonObject;
import com.couchbase.client.java.query.N1qlQuery;
import com.couchbase.client.java.query.N1qlQueryResult;
import com.couchbase.client.java.query.N1qlQueryRow;
import com.couchbase.client.java.query.Statement;
import com.fasterxml.jackson.databind.ObjectMapper;
import mongoPoC.mongoPoC.model.ClaimDocument;
import org.springframework.data.couchbase.core.CouchbaseTemplate;
import org.springframework.stereotype.Repository;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

import static com.couchbase.client.java.query.Select.select;
import static com.couchbase.client.java.query.dsl.Expression.i;
import static com.couchbase.client.java.query.dsl.Expression.s;
import static com.couchbase.client.java.query.dsl.Expression.x;
import static com.couchbase.client.java.query.dsl.functions.Collections.anyIn;
import static com.mongodb.client.model.Filters.eq;

@Repository
public class ClaimRepositoryCouchbase {

    private final CouchbaseTemplate claimCouchbaseTemplate;
    private final ObjectMapper objectMapper;


    public ClaimRepositoryCouchbase(CouchbaseTemplate claimCouchbaseTemplate, ObjectMapper objectMapper) {
        this.claimCouchbaseTemplate = claimCouchbaseTemplate;
        this.objectMapper = objectMapper;
    }


    public void insert(ClaimDocument order) {
        claimCouchbaseTemplate.insert(order);
    }

    public void save(ClaimDocument claimDocument) {
        claimCouchbaseTemplate.save(claimDocument);
    }

    public ClaimDocument fetchById(Long id) {
        return claimCouchbaseTemplate.findById(id.toString(), ClaimDocument.class);
    }


    public List<ClaimDocument> getby(List<Integer> claimItemStatuses) {
        Statement statement = select("META(claim).id, claim.*")
                .from(i("Claims"))
                .as("claim")
                .unnest("claim.claimItems").as("ci")
                .where(x("ci.claimItemStatus").in(claimItemStatuses.toString()))
                .limit(15);

        N1qlQueryResult n1qlQueryRows = claimCouchbaseTemplate.queryN1QL(N1qlQuery.simple(statement));

        var claims = n1qlQueryRows.allRows()
                .stream()
                .map(getN1qlQueryRowClaim())
                .collect(Collectors.toList());

        return claims;
    }


    public List<ClaimDocument> getByOrderLineItemId(List<Long> orderLineItemIds) {
        Statement statement = select("META(claim).id, claim.*")
                .from(i("Claims"))
                .as("claim")
                .unnest("claim.claimItems").as("ci")
                .where(x("ci.orderLineItemId").in(orderLineItemIds.toString()))
                .limit(15);

        N1qlQueryResult n1qlQueryRows = claimCouchbaseTemplate.queryN1QL(N1qlQuery.simple(statement));

        var claims = n1qlQueryRows.allRows()
                .stream()
                .map(getN1qlQueryRowClaim())
                .collect(Collectors.toList());

        return claims;
    }

    public List<ClaimDocument> getByOrderIds(List<Long> orderIds) {
        Statement statement = select("META(claim).id, claim.*")
                .from(i("Claims"))
                .as("claim")
                .where(x("claim.orderId").in(orderIds.toString()))
                .limit(15);

        N1qlQueryResult n1qlQueryRows = claimCouchbaseTemplate.queryN1QL(N1qlQuery.simple(statement));

        var claims = n1qlQueryRows.allRows()
                .stream()
                .map(getN1qlQueryRowClaim())
                .collect(Collectors.toList());

        return claims;
    }

    public  List<ClaimDocument> getBy(Long startDate, Long endDate, List<Integer> itemStatuses) {

        Statement statement = select("META(claim).id, claim.*")
                .from("Claims")
                .as("claim")
                .where(x("claim.lastModifiedDate").gte(startDate)
                    .and(x("claim.lastModifiedDate").lte(endDate))
                        .and(anyIn("item", x("claim.claimItems")).satisfies(x("item.claimItemStatus").in(itemStatuses.toString()))))
                .limit(15);

        N1qlQueryResult n1qlQueryRows = claimCouchbaseTemplate.queryN1QL(N1qlQuery.simple(statement));

        var claims = n1qlQueryRows.allRows()
                .stream()
                .map(getN1qlQueryRowClaim())
                .collect(Collectors.toList());

        return claims;
    }

    public List<ClaimDocument> getByIncrement1(Integer claimItemHistoryStatus) {
        var claims = getByClaimItemStatusHistoryStatus(1, claimItemHistoryStatus);

        claims.forEach(claimDocument -> {
            claimDocument.getClaimItems().forEach(claimItemDocument -> {
                claimItemDocument.getClaimItemStatusHistories().forEach(claimItemStatusHistoryDocument -> {
                    claimItemStatusHistoryDocument.setExecutorId(claimItemStatusHistoryDocument.getExecutorId()+1);
                });
            });
            this.save(claimDocument);
        });

        return claims;
    }


    public List<ClaimDocument> getByClaimItemStatusHistoryStatus(Integer size,Integer id) {
        Statement statement = select("META(claim).id, claim.*")
                .from(i("Claims"))
                .as("claim")
                .unnest("claim.claimItems").as("ci")
                .unnest("ci.claimItemStatusHistories").as("cish")
                .where(x("cish.claimItemStatus").eq(id))
                .limit(size);



        N1qlQueryResult n1qlQueryRows = claimCouchbaseTemplate.queryN1QL(N1qlQuery.simple(statement));

        var claims = n1qlQueryRows.allRows()
                .stream()
                .map(getN1qlQueryRowClaim())
                .collect(Collectors.toList());

        return claims;
    }

    public void clearAll() {
        var a = claimCouchbaseTemplate.getCouchbaseBucket().bucketManager().flush();
    }



    private Function<N1qlQueryRow, ClaimDocument> getN1qlQueryRowClaim() {
        return n1qlQueryRow -> {
            try {
                return objectMapper.readValue(n1qlQueryRow.byteValue(), ClaimDocument.class);
            } catch (IOException e) {
                return null;
            }
        };
    }
}