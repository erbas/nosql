package mongoPoC.mongoPoC.repository;

import com.mongodb.BasicDBObject;
import com.mongodb.MongoClient;
import com.mongodb.MongoCommandException;
import com.mongodb.client.ClientSession;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.ReplaceOptions;
import com.mongodb.client.model.UpdateOptions;
import mongoPoC.mongoPoC.model.ClaimDocument;
import org.bson.conversions.Bson;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import static com.mongodb.client.model.Filters.eq;

@Repository
public class ClaimRepositoryMongo {

    private final MongoCollection<ClaimDocument> claimDocumentMongoCollection;
    private final MongoClient claimMongoClient;

    public ClaimRepositoryMongo(MongoCollection<ClaimDocument> claimDocumentMongoCollection, MongoClient claimMongoClient) {
        this.claimDocumentMongoCollection = claimDocumentMongoCollection;
        this.claimMongoClient = claimMongoClient;
    }

    public ClaimDocument getBy(Long id) throws Exception {
        var claim = claimDocumentMongoCollection.find(eq("_id", id)).first();
        if (Objects.isNull(claim))
            throw new Exception(String.format("Claim with id %s is not found", id));
        return claim;
    }

    public List<ClaimDocument> getByOrderLineItemIds(List<Long> orderLineItemIds) throws Exception {
        List<Bson> filters = new ArrayList<>();
        filters.add(Filters.in("claimItems.orderLineItemId", orderLineItemIds));
        var claims = new ArrayList<ClaimDocument>();
        claimDocumentMongoCollection.find(Filters.and(filters))
                .limit(15)
                .into(claims);
        return claims;
    }

    public List<ClaimDocument> getByOrderIds(List<Long> orderIds) throws Exception {
        List<Bson> filters = new ArrayList<>();
        filters.add(Filters.in("orderId", orderIds));
        var claims = new ArrayList<ClaimDocument>();
        claimDocumentMongoCollection.find(Filters.and(filters))
                .limit(15)
                .into(claims);
        return claims;
    }

    public List<ClaimDocument> getBy(List<Integer> claimItemStatus) {
        List<Bson> filters = new ArrayList<>();
        filters.add(Filters.in("claimItems.claimItemStatus", claimItemStatus));
        var claims = new ArrayList<ClaimDocument>();
        claimDocumentMongoCollection.find(Filters.and(filters))
                .limit(15)
                .into(claims);
        return claims;
    }

    public List<ClaimDocument> getBy(Long startDate, Long endDate, List<Integer> itemStatuses) {

        List<Bson> filters = new ArrayList<>();
        filters.add(Filters.gte("lastModifiedDate", startDate));
        filters.add(Filters.lte("lastModifiedDate", endDate));
        filters.add(Filters.in("claimItems.claimItemStatus", itemStatuses));

        var claims = new ArrayList<ClaimDocument>();
        claimDocumentMongoCollection.find(Filters.and(filters))
                .limit(15)
                .into(claims);
        return claims;
    }

    public ArrayList<ClaimDocument> getByClaimItemStatusHistoryStatus(Integer status) {
        var claims = new ArrayList<ClaimDocument>();
//        var totalCount = claimDocumentMongoCollection.countDocuments(eq("claimItems.claimItemStatusHistories.claimItemStatus", status));

        claimDocumentMongoCollection.find(eq("claimItems.claimItemStatusHistories.claimItemStatus", status))
                .limit(15)
                .into(claims);

        return claims;
    }


    public ArrayList<ClaimDocument> getByClaimItemStatusHistoryStatusandIncrement1(Integer status) {
        var claims = new ArrayList<ClaimDocument>();
        claimDocumentMongoCollection.find(eq("claimItems.claimItemStatusHistories.claimItemStatus", status))
                .limit(1)
                .into(claims);

        claims.forEach(claimDocument -> {
            claimDocument.getClaimItems().forEach(claimItemDocument -> {
                claimItemDocument.getClaimItemStatusHistories().forEach(claimItemStatusHistoryDocument -> {
                    claimItemStatusHistoryDocument.setExecutorId(claimItemStatusHistoryDocument.getExecutorId() + 1);
                });
            });
            this.save(claimDocument);
        });
        return claims;
    }

    public ArrayList<ClaimDocument> getByClaimItemStatusHistoryStatusandIncrement1WithTx(Integer status) {
        var claims = new ArrayList<ClaimDocument>();
//        var totalCount = claimDocumentMongoCollection.countDocuments(eq("claimItems.claimItemStatusHistories.claimItemStatus", status));
        ClientSession session = claimMongoClient.startSession();
        try {
            session.startTransaction();
            claimDocumentMongoCollection.find(eq("claimItems.claimItemStatusHistories.claimItemStatus", status))
                    .limit(1)
                    .into(claims);

            claims.forEach(claimDocument -> {
                claimDocument.getClaimItems().forEach(claimItemDocument -> {
                    claimItemDocument.getClaimItemStatusHistories().forEach(claimItemStatusHistoryDocument -> {
                        claimItemStatusHistoryDocument.setExecutorId(claimItemStatusHistoryDocument.getExecutorId() + 1);
                    });
                });
                this.saveWithTx(session, claimDocument);
            });
            session.commitTransaction();
        }
        catch (MongoCommandException e) {
            session.abortTransaction();
            e.printStackTrace();
        }
        return claims;
    }

    public void insert(ClaimDocument document) {
        claimDocumentMongoCollection.insertOne(document);
    }


    public void save(ClaimDocument document) {
        Object id = document.getId();
        if (id == null) {
            claimDocumentMongoCollection.insertOne(document);
        } else {
            claimDocumentMongoCollection.replaceOne(eq("_id", id), document, new ReplaceOptions().upsert(true));
        }
    }

    public void saveWithTx(ClientSession session, ClaimDocument document) {
        Object id = document.getId();
        if (id == null) {
            claimDocumentMongoCollection.insertOne(document);
        } else {
            claimDocumentMongoCollection.replaceOne(session, eq("_id", id), document);
        }
    }

    public void clearAll() {
        var b= claimDocumentMongoCollection.deleteMany(new BasicDBObject());
    }


}
